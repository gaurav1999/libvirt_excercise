/*
 * synonyms_impl.c: Implementation of synonyms functions
 *
 * Before modifying this file read README.
 */

#include <stdlib.h>
#include <stdarg.h>
#include <glib.h>
#include <glib/gi18n.h>

#include "synonyms_impl.h"

struct _Synonyms {
    GHashTable *dictionary;
};


/**
 * synonyms_init:
 *
 * Returns: an instance of synonyms dictionary, or
 *          NULL on error (with errno set properly).
 */
Synonyms *
synonyms_init(void)
{
    Synonyms *self = g_new0 (Synonyms, 1);
    self->dictionary = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
    return self;
}


/**
 * synonyms_free:
 * @s: instance of synonyms dictionary
 *
 * Frees previously allocated dictionary. If @s is NULL then this
 * is NO-OP.
 */
void
synonyms_free(Synonyms *s)
{
    g_hash_table_destroy (s->dictionary);
    g_free (s);
}


/**
 * synonyms_define:
 * @s: instance of synonyms dictionary
 * @word: a word to add to the dictionary
 * @args: a list of synonyms
 *
 * For given @word, add it to the dictionary and define its synonyms. If the
 * @word already exists in the dictionary then just extend its list of
 * synonyms.
 *
 * Returns 0 on success, -1 otherwise.
 */
int
synonyms_define(Synonyms *s,
                const char *word, ...)
{
    gchar* key = g_strdup (word);
    gchar* value = NULL;
    gchar* k_val = NULL;

    va_list argument_list;
    va_start(argument_list, word);
    while((value = va_arg(argument_list, char*)) != NULL)
    {
        if ((k_val = g_hash_table_lookup (s->dictionary, (gpointer) key)) != NULL)
        {
            if (!g_hash_table_insert (s->dictionary, (gpointer) g_strdup (value), (gpointer) g_strdup (k_val)))
                return -1;
            if (g_hash_table_replace (s->dictionary, (gpointer) g_strdup (key), (gpointer) g_strdup (value)))
                return -1;
        }
        else if ((k_val = g_hash_table_lookup (s->dictionary, (gpointer) value)) != NULL)
        {
            if (!g_hash_table_insert (s->dictionary, (gpointer) g_strdup (key), (gpointer) g_strdup (k_val)))
                return -1;
            if (g_hash_table_replace (s->dictionary, (gpointer) g_strdup (value), (gpointer) g_strdup  (key)))
                return -1;
        }
        else if (!g_hash_table_insert (s->dictionary, (gpointer) g_strdup (key), (gpointer) g_strdup (value)))
            return -1;

        key = value;
    }
    va_end(argument_list);
    if ((g_hash_table_lookup (s->dictionary, (gpointer) key)) == NULL)
        g_hash_table_insert (s->dictionary, (gpointer) g_strdup (key), (gpointer) g_strdup (word));

    return 0;
}

/*
 * is_synonym:
 * @s: instance of synonyms dictionary
 * @w1: a word
 * @w2: a word
 *
 * Checks whether @w1 is defined synonym of @w2 (or vice versa).
 */
bool
is_synonym(Synonyms *s,
           const char *w1,
           const char *w2)
{
    gchar* key = g_strdup (w1);
    gchar* k_val = NULL;

    while ((k_val = g_hash_table_lookup (s->dictionary, (gpointer) key)) != NULL && g_strcmp0 (w1, w2))
    {
        if (!g_strcmp0(k_val, w2))
            return true;
        else if (!g_strcmp0(k_val, w1))
            break;
        else
            key = k_val;
    }

    return false;
}


/**
 * synonyms_get:
 * @s: instance of synonyms dictionary
 * @word: a word
 *
 * Returns: a string list of defined synonyms for @word, or
 *          NULL if no synonym was defined or an error occurred.
 */
char **
synonyms_get(Synonyms *s,
             const char *word)
{
    gchar* key = g_strdup (word);
    gchar* k_val = NULL;
    gchar** values = NULL;
    GArray *garray = g_array_new (FALSE, FALSE, sizeof (gchar*));
    guint length = 1;

    while ((k_val = g_hash_table_lookup (s->dictionary, (gpointer) (key))) != NULL && g_strcmp0 (k_val, word))
    {
        length++;
        g_array_append_val (garray, k_val);
        key = k_val;
    }
    if (length == 1) {
        goto cleanup;
    }
    values = g_new0(gchar*, length);
    guint count = 0;
    for (int element = 0; element < length; element++)
    {
        values[count] = g_strdup (g_array_index (garray, gchar*, element));
        count++;
    }
    cleanup:
        g_array_free (garray, TRUE);
        return values;
}
